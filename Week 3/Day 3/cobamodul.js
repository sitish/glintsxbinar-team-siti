// import modul
const modul = require('./module/module.js');

// instansiasi modul
const hitung = new modul()

/* Use hitung */

// Menghitung Luas
console.log(hitung.menghitungLuasPersegi(11))
console.log(hitung.menghitungLuasPersegiPanjang(20, 20));
console.log(hitung.menghitungLuasLingkaran(14));

// Menghitung Keliling
console.log(hitung.menghitungKelilingPersegi(40));
console.log(hitung.menghitungKelilingPersegiPanjang(100, 50));
console.log(hitung.menghitungKelilingLingkaran(21));
// hitung.hello() // error because method is private

//Menghitung Volume
console.log(hitung.menghitungVolumeBola(7)+'\n');
console.log(hitung.menghitungVolumeKerucut(21,20)+'\n');

//Menghitung Luas Permukaan
console.log(hitung.menghitungLuasPermukaanBola(10)+'\n')
console.log(hitung.menghitungLuasPermukaanKerucut(21,20)+'\n')

/* End use hitung */


console.log(hitung.menghitungVolumeKubus(20));
// Persegi 1
let sisi = 50
let luaspersegi1 = hitung.menghitungLuasPersegi(sisi)
let kelilingpersegi1 = hitung.menghitungKelilingPersegi(sisi)

console.log(luaspersegi1, kelilingpersegi1);

