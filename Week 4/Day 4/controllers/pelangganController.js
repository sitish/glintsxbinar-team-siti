const connection = require('../models/connection.js') // import connection

class PelangganController {

  // Function getAll transaksi table
  async getAll(req, res) {
    try {
      var sql = "SELECT * from pelanggan" // make an query varible

      // Run query
      connection.query(sql, function(err, result) {
        if (err) throw err; // If error

        // If success it will return JSON of result
        res.json({
          status: "success",
          data: result
        })
        return;
      });
    } catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }

  // Function getOne transaksi table
  async getOne(req, res) {
    try {
      var sql = "SELECT * FROM pelanggan WHERE id = ?" // make an query varible

      // Run query
      connection.query(sql, [req.params.id], function(err, result) {
        if (err) throw err; // If error

        // If success it will return JSON of result
        res.json({
          status: "success",
          data: result[0]
        })
        return
      });
    } catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
 }


async create(req, res) {
    try {

      var sql = 'INSERT INTO pelanggan(id, nama) VALUES (?, ?)'

      connection.query(
        sql,
        [req.body.id_pelanggan, req.body.nama],
        (err, result) => {
          if (err) {
            res.json({
              status: "Error",
              error: err
            });
            return
          } // If error

          // If success it will return JSON of result
          res.json({
            status: 'Success',
            data: result
          })
        }
      )
    } catch (err) {
      // If error will be send Error JSON
      res.json({
        status: "Error",
        error: err
      })
    }
  }
// Function update transaksi table
async update(req, res) {
    try {

      var sql = 'UPDATE pelanggan SET nama = ? WHERE id = ?'

      connection.query(
        sql,
        [req.body.nama, req.params.id],
        (err, result) => {
          if (err) {
            res.json({
              status: "Error",
              error: err
            });
            return;
          } // If error

          // If success it will return JSON of result
          res.json({
            status: 'Success',
            data: result
          })
        }
      )
    } catch (err) {
      // If error will be send Error JSON
      res.json({
        status: "Error",
        error: err
      })
    }
  }
  // Function delete transaksi table
  async delete(req, res) {
    try {

      var sql = 'DELETE FROM pelanggan WHERE id = ?'

      connection.query(
        sql,
        [req.params.id],
        (err, result) => {
          if (err) {
            res.json({
              status: "Error",
              error: err
            });
          } // If error

          // If success it will return JSON of result
          res.json({
            status: 'Success',
            data: result
          })
        }
      )
    } catch (err) {
      // If error will be send Error JSON
      res.json({
        status: "Error",
        error: err
      })
    }
  }


}


module.exports = new PelangganController;