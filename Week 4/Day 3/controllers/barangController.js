const connection = require('../models/connection.js') // import connection

class BarangController {

// Function getAll transaksi table
async getAll(req, res) {
    try {
      // SELECT ALL transaksi data
      var sql = "SELECT barang.id,barang.nama as nama_barang, barang.harga , pemasok.nama as pemasok FROM barang JOIN pemasok ON barang.id_pemasok =pemasok.id"

      // Run query
      connection.query(sql, function(err, result) {
        if (err) {
          return res.json({
            status: "Error",
            error: err
          });
        
        } // If error

        // If success it will return JSON of result
        return res.json({
          status: "success",
          data: result
        })
      });
    } catch (err) {
      // If error will be send Error JSON
      return res.json({
        status: "Error",
        error: err
      })
    }
  }

  // Function getOne transaksi table
  async getOne(req, res) {
    try {
      // SELECT data from transaksi table where id from params
      var sql = "SELECT barang.id,barang.nama as nama_barang, barang.harga , pemasok.nama as pemasok FROM barang JOIN pemasok ON barang.id_pemasok =pemasok.id WHERE barang.id = ?" // make an query varible

      // Run query
      connection.query(sql, [req.params.id], function(err, result) {
        if (err) {
          return res.json({
            status: "Error",
            error: err
          });
        } // If error

        // If success it will return JSON of result
        else{return res.json({
          status: "success",
          data: result
        })}
      });
    } catch (err) {
      // If error will be send Error JSON
      res.json({
        status: "Error",
        error: err
      })
    }
  }

  // Function create transaksi table
  async create(req, res) {
    try {

      var sql = 'INSERT INTO barang(nama, harga, id_pemasok) VALUES ( ?, ?, ?)'

      connection.query(
        sql,
        [req.body.nama, req.body.harga, req.body.id_pemasok],
        (err, result) => {
          if (err) {
            return res.json({
              status: "Error",
              error: err
            });
            
          } // If error

          // If success it will return JSON of result
          return res.json({
            status: 'Success',
            data: result
          })
        }
      )
    } catch (err) {
      // If error will be send Error JSON
      return res.json({
        status: "Error",
        error: err
      })
    }
  }
// Function update transaksi table
async update(req, res) {
    try {

      var sql = 'UPDATE barang b SET nama = ?, harga = ?, id_pemasok = ? WHERE id = ?'

      connection.query(
        sql,
        [ req.body.nama, req.body.harga, req.body.id_pemasok, req.params.id],
        (err, result) => {
          if (err) {
            return res.json({
              status: "Error",
              error: err
            });
          } // If error

          // If success it will return JSON of result
          return res.json({
            status: 'Success',
            data: result
          })
        }
      )
    } catch (err) {
      // If error will be send Error JSON
      res.json({
        status: "Error",
        error: err
      })
    }
  }
  // Function delete transaksi table
  async delete(req, res) {
    try {

      var sql = 'DELETE FROM barang WHERE id = ?'

      connection.query(
        sql,
        [req.params.id],
        (err, result) => {
          if (err) {
            return res.json({
              status: "Error",
              error: err
            });
          } // If error

          // If success it will return JSON of result
          return res.json({
            status: 'Success',
            data: result
          })
        }
      )
    } catch (err) {
      // If error will be send Error JSON
      res.json({
        status: "Error",
        error: err
      })
    }
  }


}

module.exports = new BarangController;
