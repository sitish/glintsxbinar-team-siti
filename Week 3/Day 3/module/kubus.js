// Import bangun datar
const bangunruang = require('./bangunruang.js');

// Make Kubus class inherit from BangunRuang
class Kubus extends bangunruang {
  constructor(sisi2) {
    super('Kubus')

    this.sisi2 = sisi2
  }

  // override menghitungVolume BangunRuang class
  menghitungVolume() {
    // super.menghitungVolume()
    return Math.pow(this.sisi2, 2) * 6
  }

//   // override menghitungKeliling BangunDatar class
//   menghitungKeliling() {
//     return 4 * this.sisi
//   }
}

module.exports = Kubus;
