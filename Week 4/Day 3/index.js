const express = require('express') // Import Express
const app = express() // Create app from express
const transaksiRoutes = require('./routes/transaksiRouter.js') // Import transaksiRoutes
const barangRoutes = require('./routes/barangRouter.js')
const pelangganRoutes = require('./routes/pelangganRouter.js')


app.use(express.urlencoded({extended:false}))

app.use('/transaksi', transaksiRoutes) // If accessing localhost:3000/transaksi/*, it will use transaksiRoutes
app.use('/barang', barangRoutes) 
app.use('/pelanggan', pelangganRoutes) 


app.listen(8000) // make application have port 3000
