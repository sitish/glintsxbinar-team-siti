/**
 *  SEMUA 2D TIDAK ADA VALIDASI isNaN
 * operation==1 artinya mencari luas permukaan
 * operation==2 artinya mencari volume
 * utk CONE dan SPHERE ga ada fungsinya disini udah terlanjur buat di task kmren
 */
const index = require('./index.js')


function circle(operation){
    if(operation==1){
      index.rl.question("radius:",rad=>{
        console.log(`Result: ${3.14*(rad**2)}`);
        index.rl.close()
      })    
    }else if(operation==2){
      index.rl.question("radius",rad=>{
        console.log(`Result: ${3.14*2*rad}`)
        index.rl.close()
      })
    }
  }

function square(operation){
    if(operation==1){
      index.rl.question("side length: ",length=>{
        console.log(`Result: ${length**2}`);
        index.rl.close()
      })    
    }else if(operation==2){
      index.rl.question("side length: ",length=>{
        console.log(`Result: ${4*length}`);
        index.rl.close()
      })
    }
  }

function rectangle(operation){
  if(operation==1){
    index.rl.question("length:",length=>{
      index.rl.question("width:",width=>{
        console.log(`Result: ${length*width}`);
        index.rl.close()
      })
    })
  }else if(operation==2){
    index.rl.question("length:",length=>{
      index.rl.question("width:",width=>{
        console.log(`Result: ${2*(length+width)}`)
        index.rl.close()
      })
    })
  }
}
function parallelogram(operation){
  if(operation==1){
    index.rl.question("base:",base=>{
      index.rl.question("height:",height=>{
        console.log(`Result: ${base*height}`);
        index.rl.close()
      })
    })
  }else if(operation==2){
    index.rl.question("length a (side):",length=>{
      index.rl.question("length b (base):",base=>{
        console.log(`Result: ${2*(length+base)}`)
        index.rl.close()
      })
    })
  }
}
module.exports.circle = circle;
module.exports.square = square;
module.exports.rectangle = rectangle;
module.exports.parallelogram = parallelogram;
