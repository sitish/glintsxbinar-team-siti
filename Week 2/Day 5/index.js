const data = require('./lib/arrayFactory.js');
const test = require('./lib/test.js');

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter(i => typeof i === 'number');
}

// Should return array
function sortAscending(data) {
  // Code Here
  newdata=clean(data);
  var temp=[]
  for(let i=0;i<newdata.length;i++){
    for(let j=0;j<newdata.length;j++){
      if(newdata[j]>newdata[j+1]){
        temp=newdata[j]
        newdata[j]=newdata[j+1]
        newdata[j+1]=temp

      }
    }
  }

  return newdata;
}

// Should return array
function sortDecending(data) {
  // Code Here
  newdata=clean(data);
  var temp=[]
  for(let i=0;i<newdata.length;i++){
    for(let j=0;j<newdata.length;j++){
      if(newdata[j]<newdata[j+1]){
        temp=newdata[j]
        newdata[j]=newdata[j+1]
        newdata[j+1]=temp

      }
    }
  }

  return newdata;

}

// DON'T CHANGE
test(sortAscending, sortDecending, data);
