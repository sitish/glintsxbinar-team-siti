const mysql = require('mysql') // Import mysql

// Make mysql connection
const connection = mysql.createConnection({
  host: 'localhost',
  user: 'sitish',
  password: 'password',
  database: 'penjualan1'
})

module.exports = connection; // export connection
